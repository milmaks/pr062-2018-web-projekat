﻿using PR62_2018_TuristickaAgencija.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_TuristickaAgencija.Controllers
{
    public class ManagerController : Controller
    {
        // GET: Manager
        public ActionResult Index()
        {
            User user = Session["User"] as User;
            List<Aranzman> aranzmaniManagera = new List<Aranzman>();

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            if (HttpContext.Application["aranzmaniManagera"] == null)
            {
                HttpContext.Application["aranzmaniManagera"] = user.ListaAranzmana;
                aranzmaniManagera = user.ListaAranzmana;
            }
            else
            {
                aranzmaniManagera = HttpContext.Application["aranzmaniManagera"] as List<Aranzman>;
            }

            return View(aranzmaniManagera);
        }

        public ActionResult NoviAranzman()
        {
            User user = Session["User"] as User;
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");


            Aranzman aranzman = new Aranzman();
            List<Smestaj> forCheck = new List<Smestaj>();
            foreach (var smestaj in smestaji)
            {
                if (smestaj.Active)
                    forCheck.Add(smestaj);
            }
            ViewBag.Smestaji = forCheck;
            ViewBag.Message = TempData["errmsg"] ?? "Popunite sva polja da bi ste kreirali novi aranzman";

            return View(aranzman);
        }

        public ActionResult NoviSmestaj()
        {
            User user = Session["User"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            Smestaj smestaj = new Smestaj();
            ViewBag.Message = TempData["errmsg"] ?? "Popunite sva polja da bi ste kreirali novi smestaj";

            return View(smestaj);
        }

        public ActionResult SmestajnaJed()
        {
            User user = Session["User"] as User;
            List<Smestaj> smestaji = HttpContext.Application["smestajnejed"] as List<Smestaj>;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            if (TempData["idSmestaj"] == null && TempData["indexi"] == null)
                return RedirectToAction("SmestajneJed", "Home");

            SmestajnaJedinica jedinica = new SmestajnaJedinica();
            ViewBag.Message = TempData["errmsg"] ?? "Popunite sva polja da bi ste dodali novu smestajnu jedinicu";
            if(TempData["izmena"] != null)
            {
                ViewBag.Smestaj = smestaji[Convert.ToInt32(((string)TempData["indexi"]).Split('|')[0])];
                ViewBag.Index = Convert.ToInt32(((string)TempData["indexi"]).Split('|')[0]);
                ViewBag.IndexJed = Convert.ToInt32(((string)TempData["indexi"]).Split('|')[1]);
                jedinica = smestaji[Convert.ToInt32(((string)TempData["indexi"]).Split('|')[0])].SmestajneJedinice[Convert.ToInt32(((string)TempData["indexi"]).Split('|')[1])];
                List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;

                foreach (var aranzman in aranzmani)
                {
                    if (aranzman.Active && aranzman.DatumPocetka >= DateTime.Now)
                    {
                        if (aranzman.Smestaj.Equals(ViewBag.Smestaj))
                        {
                            if(aranzman.Smestaj.SmestajneJedinice[Convert.ToInt32(((string)TempData["indexi"]).Split('|')[1])].Reserverd)
                            {
                                ViewBag.Message = "Ne mozete izmeniti broj kreveta jer postoji rezervacija ove jedinice";
                            }
                        }   
                    }
                }
                ViewBag.Kontrola = "izmena";
            }
            else
            {
                ViewBag.Smestaj = smestaji[Convert.ToInt32(TempData["idSmestaj"])];
                ViewBag.Index = TempData["idSmestaj"];
                ViewBag.Kontrola = "dodavanje";
            }
            return View(jedinica);
        }

        public ActionResult IzmenaAranzmana()
        {
            User user = Session["User"] as User;
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;
            List<Aranzman> aranzmani = HttpContext.Application["aranzmaniManagera"] as List<Aranzman>;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            if (TempData["index"] == null)
                return RedirectToAction("Index");

            Aranzman aranzman = aranzmani[Convert.ToInt32(TempData["index"])];
            ViewBag.Smestaji = smestaji;
            ViewBag.Message = TempData["errmsg"] ?? "Popunite sva polja da bi ste izmenili aranzman, upload slike je neophodan samo u slucaju da je zelite promeniti";
            ViewBag.IndexLista = aranzmani.IndexOf(aranzman);
            ViewBag.Index = TempData["index"];

            return View(aranzman);
        }

        public ActionResult IzmenaSmestaja()
        {
            User user = Session["User"] as User;
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            if (TempData["index"] == null)
                return RedirectToAction("Smestaji");

            Smestaj smestaj = smestaji[Convert.ToInt32(TempData["index"])];
            ViewBag.Message = TempData["errmsg"] ?? "Sva polja moraju biti popunjena da bi smestaj bio promenjen";
            ViewBag.Index = TempData["index"];

            return View(smestaj);
        }

        public ActionResult Smestaji()
        {
            User user = Session["User"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;
            Dictionary<Smestaj, int> zauzetosti = new Dictionary<Smestaj, int>(smestaji.Count);
            foreach (var item in smestaji)
            {
                if(item.Active)
                    zauzetosti.Add(item, 0);
            }
            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            ViewBag.Brisanje = "";
            foreach (var aranzman in aranzmani)
            {
                if (aranzman.Active)
                {
                    if (DateTime.Now < aranzman.DatumPocetka || (DateTime.Now >= aranzman.DatumPocetka && DateTime.Now <= aranzman.DatumZavrsetka))
                    {
                        ViewBag.Brisanje += smestaji.IndexOf(aranzman.Smestaj).ToString();
                        ViewBag.Brisanje += ",";

                        if (!zauzetosti.ContainsKey(aranzman.Smestaj))
                        {
                            int br = 0;
                            foreach (var jed in aranzman.Smestaj.SmestajneJedinice)
                            {
                                if (jed.Reserverd)
                                    br++;
                            }
                            zauzetosti[aranzman.Smestaj] = br;
                        }
                    }
                }
            }

            ViewBag.Zauzeti = zauzetosti;
            return View(smestaji);
        }

        public ActionResult Rezervacije()
        {
            User user = Session["User"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            List<Rezervacija> rezervacije = HttpContext.Application["rezervacije"] as List<Rezervacija>;
            List<Rezervacija> ret = new List<Rezervacija>();
            if (TempData["rezervacije"] == null)
            {
                foreach (var rezervacija in rezervacije)
                {
                    if (user.ListaAranzmana.Contains(rezervacija.Aranzman))
                        ret.Add(rezervacija);
                }
            }
            else
                ret = HttpContext.Application["rezervacije"] as List<Rezervacija>;

            return View(ret);
        }

        public ActionResult Rezervacija()
        {
            User user = Session["User"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.MENADZER)
                return RedirectToAction("Index", "Home");

            if (TempData["id"] == null)
                return RedirectToAction("Rezervacije");

            List<Rezervacija> rezervacije = HttpContext.Application["rezervacije"] as List<Rezervacija>;
            Rezervacija rezervacija = null;

            foreach (var rez in rezervacije)
            {
                rezervacija = rez;
            }

            return View(rezervacija);
        }

        [HttpPost]
        public ActionResult AddSmestajnaJed(SmestajnaJedinica jedinica, int indexSmestaj)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestajnejed"] as List<Smestaj>;
            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;

            jedinica.Active = true;
            foreach (var aranzman in aranzmani)
            {
                if (aranzman.Active && (aranzman.DatumPocetka > DateTime.Now || (aranzman.DatumPocetka <= DateTime.Now && aranzman.DatumZavrsetka >= DateTime.Now)))
                {
                    if (aranzman.Smestaj.Equals(smestaji[indexSmestaj]))
                        aranzman.Smestaj.SmestajneJedinice.Add(jedinica);
                }
            }

            smestaji[indexSmestaj].SmestajneJedinice.Add(jedinica);
            DB.Save<Smestaj>(smestaji);
            DB.Save<Aranzman>(aranzmani);
            HttpContext.Application["smestajnejed"] = smestaji;
            HttpContext.Application["aranzmani"] = aranzmani;

            return RedirectToAction("SmestajneJed", "Home");
        }

        [HttpPost]
        public ActionResult ChangeSmestajnaJed(SmestajnaJedinica jedinica, int indexSmestaj, int indexJed)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestajnejed"] as List<Smestaj>;
            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            List<Smestaj> smestajiDB = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            List<Aranzman> aranzmaniDB = DB.Load<Aranzman>("~/App_Data/aranzmani.txt");

            foreach (var aranzman in aranzmaniDB)
            {
                if (aranzman.Active && (aranzman.DatumPocetka > DateTime.Now || (aranzman.DatumPocetka <= DateTime.Now && aranzman.DatumZavrsetka >= DateTime.Now)))
                {
                    if (aranzman.Smestaj.Equals(smestaji[indexSmestaj]))
                    {
                        foreach (var jedinicaAran in aranzman.Smestaj.SmestajneJedinice)
                        {
                            if(jedinicaAran.Equals(smestaji[indexSmestaj].SmestajneJedinice[indexJed]))
                            {
                                jedinicaAran.MaxGostiju = jedinica.MaxGostiju;
                                jedinicaAran.PetFriendly = jedinica.PetFriendly;
                                jedinicaAran.Cena = jedinica.Cena;
                                break;
                            }
                        }
                    }
                }
            }
            DB.Save<Aranzman>(aranzmaniDB);

            foreach (var smestaj in smestajiDB)
            {
                if(smestaj.Equals(smestaji[indexSmestaj]))
                {
                    foreach (var jedinicaSmes in smestaj.SmestajneJedinice)
                    {
                        if (jedinicaSmes.Equals(smestaji[indexSmestaj].SmestajneJedinice[indexJed]))
                        {
                            jedinicaSmes.MaxGostiju = jedinica.MaxGostiju;
                            jedinicaSmes.PetFriendly = jedinica.PetFriendly;
                            jedinicaSmes.Cena = jedinica.Cena;
                            break;
                        }
                    }
                    
                }
            }
            DB.Save<Smestaj>(smestajiDB);

            foreach (var aranzman in aranzmani)
            {
                if (aranzman.Active && (aranzman.DatumPocetka > DateTime.Now || (aranzman.DatumPocetka <= DateTime.Now && aranzman.DatumZavrsetka >= DateTime.Now)))
                {
                    if (aranzman.Smestaj.Equals(smestaji[indexSmestaj]))
                    {
                        aranzman.Smestaj.SmestajneJedinice[indexJed].MaxGostiju = jedinica.MaxGostiju;
                        aranzman.Smestaj.SmestajneJedinice[indexJed].PetFriendly = jedinica.PetFriendly;
                        aranzman.Smestaj.SmestajneJedinice[indexJed].Cena = jedinica.Cena;
                    }
                }
            }

            smestaji[indexSmestaj].SmestajneJedinice[indexJed].MaxGostiju = jedinica.MaxGostiju;
            smestaji[indexSmestaj].SmestajneJedinice[indexJed].PetFriendly = jedinica.PetFriendly;
            smestaji[indexSmestaj].SmestajneJedinice[indexJed].Cena = jedinica.Cena;
            
            HttpContext.Application["smestajnejed"] = smestaji;
            HttpContext.Application["aranzmani"] = aranzmani;

            return RedirectToAction("SmestajneJed", "Home");
        }
        
        [HttpPost]
        public ActionResult DelSmestajnaJed(string indexi)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestajnejed"] as List<Smestaj>;
            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            List<Smestaj> smestajiDB = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            List<Aranzman> aranzmaniDB = DB.Load<Aranzman>("~/App_Data/aranzmani.txt");

            int indexSmestaj = Convert.ToInt32(indexi.Split('|')[0]);
            int indexJed = Convert.ToInt32(indexi.Split('|')[1]);

            foreach (var aranzman in aranzmaniDB)
            {
                if (aranzman.Active && (aranzman.DatumPocetka > DateTime.Now || (aranzman.DatumPocetka <= DateTime.Now && aranzman.DatumZavrsetka >= DateTime.Now)))
                {
                    if (aranzman.Smestaj.Equals(smestaji[indexSmestaj]))
                    {
                        foreach (var jedinicaAran in aranzman.Smestaj.SmestajneJedinice)
                        {
                            if (jedinicaAran.Equals(smestaji[indexSmestaj].SmestajneJedinice[indexJed]))
                            {
                                jedinicaAran.Active = false;
                                break;
                            }
                        }
                    }
                }
            }
            DB.Save<Aranzman>(aranzmaniDB);

            foreach (var smestaj in smestajiDB)
            {
                if (smestaj.Equals(smestaji[indexSmestaj]))
                {
                    foreach (var jedinicaSmes in smestaj.SmestajneJedinice)
                    {
                        if (jedinicaSmes.Equals(smestaji[indexSmestaj].SmestajneJedinice[indexJed]))
                        {
                            jedinicaSmes.Active = false;
                            break;
                        }
                    }

                }
            }
            DB.Save<Smestaj>(smestajiDB);

            foreach (var aranzman in aranzmani)
            {
                if (aranzman.Active && (aranzman.DatumPocetka > DateTime.Now || (aranzman.DatumPocetka <= DateTime.Now && aranzman.DatumZavrsetka >= DateTime.Now)))
                {
                    if (aranzman.Smestaj.Equals(smestaji[indexSmestaj]))
                    {
                        aranzman.Smestaj.SmestajneJedinice[indexJed].Active = false;
                    }
                }
            }

            smestaji[indexSmestaj].SmestajneJedinice[indexJed].Active = false;
            
            HttpContext.Application["smestajnejed"] = smestaji;
            HttpContext.Application["aranzmani"] = aranzmani;

            return RedirectToAction("SmestajneJed", "Home");
        }

        [HttpPost]
        public ActionResult AddAranzman(Aranzman aranzman, string smestaj, string ulica, int broj, string gradmesto, int post, string geoSirina, string geoDuzina, int sat, int min, HttpPostedFileBase file)
        {
            User user = Session["User"] as User;
            List<Aranzman> aranzmani = DB.Load<Aranzman>("~/App_Data/aranzmani.txt");
            List<Smestaj> smestaji = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            List<User> users = HttpContext.Application["users"] as List<User>;

            string adresa = ulica + " " + broj.ToString() + ", " + gradmesto + ", " + post.ToString();
            MestoNalazenja mestoNalazenja = new MestoNalazenja(adresa, geoSirina, geoDuzina);
            int index = 0;
            foreach (var item in smestaji)
            {
                if(item.Naziv.Equals(smestaj) && item.Active)
                {
                    break;
                }
                index++;
            }

            if (sat >= 10 && min >= 10)
                aranzman.VremeNalazenja = sat.ToString() + ":" + min.ToString();
            if(sat >= 10 && min < 10)
                aranzman.VremeNalazenja = sat.ToString() + ":0" + min.ToString();
            if (sat < 10 && min >= 10)
                aranzman.VremeNalazenja = "0" + sat.ToString() + ":" + min.ToString();
            if(sat<10 && min < 10)
                aranzman.VremeNalazenja = "0" + sat.ToString() + ":0" + min.ToString();

            string fileName = Path.GetFileName(file.FileName);
            string path = Path.Combine(Server.MapPath("~/Slike/"), fileName);
            file.SaveAs(path);

            aranzman.Mesto = mestoNalazenja;
            aranzman.Path = fileName;
            if(smestaji[index].SmestajneJedinice.Count > 0)
                aranzman.Smestaj = smestaji[index];
            else
            {
                TempData["errmsg"] = "Izabrani smestaj nema smestajnih jedinica";
                return RedirectToAction("NoviAranzman");
            }
            aranzman.Active = true;

            foreach (var item in users)
            {
                if (item.Username.Equals(user.Username))
                {
                    item.ListaAranzmana.Add(aranzman);
                    break;
                }
            }
            //user.ListaAranzmana.Add(aranzman);
            HttpContext.Application["aranzmaniManagera"] = user.ListaAranzmana;
            aranzmani.Add(aranzman);

            DB.Save<User>(users);
            DB.Save<Aranzman>(aranzmani);
            Session["user"] = user;
            HttpContext.Application["aranzmani"] = aranzmani;

            return RedirectToAction("Index");
        }

        
        [HttpPost]
        public ActionResult ChangeAranzmanInfo(Aranzman aranzman, string smestaj, string ulica, int broj, string gradmesto, int post, string geoSirina, string geoDuzina, int sat, int min, HttpPostedFileBase file, int indexAranzman, int indexUser)
        {
            User user = Session["User"] as User;
            List<Aranzman> aranzmani = DB.Load<Aranzman>("~/App_Data/aranzmani.txt");
            List<Aranzman> aranzmaniManager = HttpContext.Application["aranzmaniManagera"] as List<Aranzman>;
            List<Smestaj> smestaji = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            List<Rezervacija> rezervacije = DB.Load<Rezervacija>("~/App_Data/rezervacije.txt");
            List<User> users = HttpContext.Application["users"] as List<User>;
            indexAranzman = aranzmani.IndexOf(aranzmaniManager[indexAranzman]);

            string adresa = ulica + " " + broj.ToString() + ", " + gradmesto + ", " + post.ToString();
            MestoNalazenja mestoNalazenja = new MestoNalazenja(adresa, geoSirina, geoDuzina);
            int index = 0;
            foreach (var item in smestaji)
            {
                if (item.Naziv.Equals(smestaj) && item.Active)
                {
                    break;
                }
                index++;
            }

            if (sat >= 10 && min >= 10)
                aranzman.VremeNalazenja = sat.ToString() + ":" + min.ToString();
            if (sat >= 10 && min < 10)
                aranzman.VremeNalazenja = sat.ToString() + ":0" + min.ToString();
            if (sat < 10 && min >= 10)
                aranzman.VremeNalazenja = "0" + sat.ToString() + ":" + min.ToString();
            if (sat < 10 && min < 10)
                aranzman.VremeNalazenja = "0" + sat.ToString() + ":0" + min.ToString();

            if (file != null)
            {
                string fileName = Path.GetFileName(file.FileName);
                string path = Path.Combine(Server.MapPath("~/Slike/"), fileName);
                file.SaveAs(path);
                aranzman.Path = fileName;
            }

            aranzman.Mesto = mestoNalazenja;

            if (smestaji[index].Active == false)
            {
                TempData["index"] = user.ListaAranzmana.IndexOf(aranzmani[indexAranzman]).ToString();
                TempData["errmsg"] = "Izabrani smestaj ne postoji";
                return RedirectToAction("IzmenaAranzmana");
            }

            if (smestaji[index].SmestajneJedinice.Count > 0)
                aranzman.Smestaj = smestaji[index];
            else
            {
                TempData["index"] = user.ListaAranzmana.IndexOf(aranzmani[indexAranzman]).ToString();
                TempData["errmsg"] = "Izabrani smestaj trenutno nema smestajnih jedinica";
                return RedirectToAction("IzmenaAranzmana");
            }

            List<string> rezToChange = new List<string>();
            foreach (var turista in users)
            {
                if (turista.Uloga == Uloga.TURISTA)
                {
                    foreach (var rez in turista.ListaRezervacija)
                    {
                        if (rez.Aranzman.Equals(aranzmani[indexAranzman]))
                        {
                            rezToChange.Add(rez.Id);
                            rez.Aranzman.Naziv = aranzman.Naziv;
                            rez.Aranzman.Tip = aranzman.Tip;
                            rez.Aranzman.Prevoz = aranzman.Prevoz;
                            rez.Aranzman.Lokacija = aranzman.Lokacija;
                            rez.Aranzman.DatumPocetka = aranzman.DatumPocetka;
                            rez.Aranzman.DatumZavrsetka = aranzman.DatumZavrsetka;
                            rez.Aranzman.Mesto = aranzman.Mesto;
                            rez.Aranzman.VremeNalazenja = aranzman.VremeNalazenja;
                            rez.Aranzman.MaxPutnika = aranzman.MaxPutnika;
                            rez.Aranzman.Opis = aranzman.Opis;
                            rez.Aranzman.ProgramPutovanja = aranzman.ProgramPutovanja;
                        }
                    }
                }
            }

            foreach (var id in rezToChange)
            {
                foreach (var rez in rezervacije)
                {
                    if(rez.Id.Equals(id))
                    {
                        rez.Aranzman.Naziv = aranzman.Naziv;
                        rez.Aranzman.Tip = aranzmani[indexAranzman].Tip = aranzman.Tip;
                        rez.Aranzman.Prevoz = aranzmani[indexAranzman].Prevoz = aranzman.Prevoz;
                        rez.Aranzman.Lokacija = aranzmani[indexAranzman].Lokacija = aranzman.Lokacija;
                        rez.Aranzman.DatumPocetka = aranzmani[indexAranzman].DatumPocetka = aranzman.DatumPocetka;
                        rez.Aranzman.DatumZavrsetka = aranzmani[indexAranzman].DatumZavrsetka = aranzman.DatumZavrsetka;
                        rez.Aranzman.Mesto = aranzmani[indexAranzman].Mesto = aranzman.Mesto;
                        rez.Aranzman.VremeNalazenja = aranzmani[indexAranzman].VremeNalazenja = aranzman.VremeNalazenja;
                        rez.Aranzman.MaxPutnika = aranzmani[indexAranzman].MaxPutnika = aranzman.MaxPutnika;
                        rez.Aranzman.Opis = aranzmani[indexAranzman].Opis = aranzman.Opis;
                        rez.Aranzman.ProgramPutovanja = aranzmani[indexAranzman].ProgramPutovanja = aranzman.ProgramPutovanja;
                        break;
                    }
                }
            }

            user.ListaAranzmana[indexUser].Naziv = aranzmani[indexAranzman].Naziv = aranzman.Naziv;
            user.ListaAranzmana[indexUser].Tip = aranzmani[indexAranzman].Tip = aranzman.Tip;
            user.ListaAranzmana[indexUser].Prevoz = aranzmani[indexAranzman].Prevoz = aranzman.Prevoz;
            user.ListaAranzmana[indexUser].Lokacija = aranzmani[indexAranzman].Lokacija = aranzman.Lokacija;
            user.ListaAranzmana[indexUser].DatumPocetka = aranzmani[indexAranzman].DatumPocetka = aranzman.DatumPocetka;
            user.ListaAranzmana[indexUser].DatumZavrsetka = aranzmani[indexAranzman].DatumZavrsetka = aranzman.DatumZavrsetka;
            user.ListaAranzmana[indexUser].Mesto = aranzmani[indexAranzman].Mesto = aranzman.Mesto;
            user.ListaAranzmana[indexUser].VremeNalazenja = aranzmani[indexAranzman].VremeNalazenja = aranzman.VremeNalazenja;
            user.ListaAranzmana[indexUser].MaxPutnika = aranzmani[indexAranzman].MaxPutnika = aranzman.MaxPutnika;
            user.ListaAranzmana[indexUser].Opis = aranzmani[indexAranzman].Opis = aranzman.Opis;
            user.ListaAranzmana[indexUser].ProgramPutovanja = aranzmani[indexAranzman].ProgramPutovanja = aranzman.ProgramPutovanja;
            if(file!=null)
                user.ListaAranzmana[indexUser].Path = aranzmani[indexAranzman].Path = aranzman.Path;
            user.ListaAranzmana[indexUser].Smestaj = aranzmani[indexAranzman].Smestaj = aranzman.Smestaj;


            DB.Save<User>(users);
            DB.Save<Aranzman>(aranzmani);
            DB.Save<Rezervacija>(rezervacije);
            Session["user"] = user;
            HttpContext.Application["aranzmani"] = aranzmani;
            HttpContext.Application["aranzmaniManagera"] = user.ListaAranzmana;
            HttpContext.Application["rezervacije"] = rezervacije;

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult IzmeniAranzman(string index)
        {
            TempData["index"] = index;
            return RedirectToAction("IzmenaAranzmana");
        }

        [HttpPost]
        public ActionResult IzmeniSmestaj(string index)
        {
            TempData["index"] = index;
            return RedirectToAction("IzmenaSmestaja");
        }

        [HttpPost]
        public ActionResult DeleteAranzman(string index)
        {
            User user = Session["User"] as User;
            List<Aranzman> aranzmaniManager = HttpContext.Application["aranzmaniManagera"] as List<Aranzman>;
            List<Aranzman> aranzmani = DB.Load<Aranzman>("~/App_Data/aranzmani.txt");
            List<User> users = HttpContext.Application["users"] as List<User>;
            int i = Convert.ToInt32(index);
            

            Aranzman a = aranzmaniManager[i];
            int indexA = aranzmani.IndexOf(a);
            int indexU = user.ListaAranzmana.IndexOf(a);

            aranzmaniManager[i].Active = false;
            aranzmani[indexA].Active = false;
            users[users.IndexOf(user)].ListaAranzmana[indexU].Active = false;
            user = users[users.IndexOf(user)];
            DB.Save<User>(users);
            DB.Save<Aranzman>(aranzmani);

            Session["user"] = user;
            HttpContext.Application["aranzmani"] = aranzmani;
            HttpContext.Application["aranzmaniManagera"] = aranzmaniManager;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddSmestaj(Smestaj smestaj)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;
            smestaj.Active = true;

            smestaji.Add(smestaj);
            DB.Save<Smestaj>(smestaji);
            HttpContext.Application["smestaji"] = smestaji;

            return RedirectToAction("Smestaji");
        }

        [HttpPost]
        public ActionResult ChangeSmestajInfo(Smestaj smestaj, int index)
        {
            User user = Session["User"] as User;
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;

            smestaji[index].Naziv = smestaj.Naziv;
            smestaji[index].BrojZvezdica = smestaj.BrojZvezdica;
            smestaji[index].Bazen = smestaj.Bazen;
            smestaji[index].Spa = smestaj.Spa;
            smestaji[index].Prilagodjen = smestaj.Prilagodjen;
            smestaji[index].Wifi = smestaj.Wifi;

            DB.Save<Smestaj>(smestaji);
            HttpContext.Application["smestaji"] = smestaji;

            return RedirectToAction("Smestaji");
        }

        [HttpPost]
        public ActionResult RemoveSmestaj(int index)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;

            smestaji[index].Active = false;
            DB.Save<Smestaj>(smestaji);
            HttpContext.Application["smestaji"] = smestaji;

            return RedirectToAction("Smestaji");
        }

        [HttpPost]
        public ActionResult SearchSmestaji(string tip, string naziv, string bazen, string spa, string prilagodjeno, string wifi)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;
            List<Smestaj> ret = new List<Smestaj>();

            Regex patern1 = new Regex(@"." + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern2 = new Regex(@"" + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern3 = new Regex(@"." + naziv, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            foreach (var smestaj in smestaji)
            {
                if (tip != "" && !smestaj.Tip.Equals(tip))
                    continue;

                if(bazen!=null)
                {
                    if (bazen.Equals("Da"))
                    {
                        if (!smestaj.Bazen)
                            continue;
                    }
                    else
                    {
                        if (smestaj.Bazen)
                            continue;
                    }
                }

                if (spa != null)
                {
                    if (spa.Equals("Da"))
                    {
                        if (!smestaj.Spa)
                            continue;
                    }
                    else
                    {
                        if (smestaj.Spa)
                            continue;
                    }
                }

                if (prilagodjeno != null)
                {
                    if (prilagodjeno.Equals("Da"))
                    {
                        if (!smestaj.Prilagodjen)
                            continue;
                    }
                    else
                    {
                        if (smestaj.Prilagodjen)
                            continue;
                    }
                }

                if (wifi != null)
                {
                    if (wifi.Equals("Da"))
                    {
                        if (!smestaj.Wifi)
                            continue;
                    }
                    else
                    {
                        if (smestaj.Wifi)
                            continue;
                    }
                }

                if (naziv != "")
                    if (!(patern1.Match(smestaj.Naziv).Success ||
                        patern2.Match(smestaj.Naziv).Success ||
                        patern3.Match(smestaj.Naziv).Success))
                        continue;

                ret.Add(smestaj);
            }

            HttpContext.Application["smestaji"] = ret;
            return RedirectToAction("Smestaji");
        }

        [HttpPost]
        public ActionResult ClearSmestaji()
        {
            List<Smestaj> smestaji = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            HttpContext.Application["smestaji"] = smestaji;
            return RedirectToAction("Smestaji");
        }

        [HttpPost]
        public ActionResult SortSmestaji(string sort, string type)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestaji"] as List<Smestaj>;
            List<Smestaj> ret = new List<Smestaj>(smestaji.Count);

            if(sort.Equals("naziv"))
            {
                List<Tuple<int, string>> nazivi = new List<Tuple<int, string>>(smestaji.Count);
                int br = 0;
                foreach (var smestaj in smestaji)
                {
                    nazivi.Add(new Tuple<int, string>(br++, smestaj.Naziv));
                }

                if (type.Equals("a"))
                    nazivi = nazivi.OrderBy(t => t.Item2).ToList();
                else
                    nazivi = nazivi.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in nazivi)
                {
                    ret.Add(smestaji[item.Item1]);
                }

            }
            else if(sort.Equals("brJed"))
            {
                List<Tuple<int, int>> brJed = new List<Tuple<int, int>>(smestaji.Count);
                int br = 0;
                foreach (var smestaj in smestaji)
                {
                    brJed.Add(new Tuple<int, int>(br++, smestaj.SmestajneJedinice.Count));
                }

                if (type.Equals("a"))
                    brJed = brJed.OrderBy(t => t.Item2).ToList();
                else
                    brJed = brJed.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in brJed)
                {
                    ret.Add(smestaji[item.Item1]);
                }
            }
            else
            {
                List<Tuple<int, int>> brJed = new List<Tuple<int, int>>(smestaji.Count);
                Dictionary<Smestaj, int> zauzetosti = new Dictionary<Smestaj, int>();
                List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
                foreach (var aranzman in aranzmani)
                {
                    if (aranzman.Active)
                    {
                        if (DateTime.Now < aranzman.DatumPocetka || (DateTime.Now >= aranzman.DatumPocetka && DateTime.Now <= aranzman.DatumZavrsetka))
                        {

                            if (!zauzetosti.ContainsKey(aranzman.Smestaj))
                            {
                                int br = 0;
                                foreach (var jed in aranzman.Smestaj.SmestajneJedinice)
                                {
                                    if (jed.Reserverd)
                                        br++;
                                }
                                zauzetosti.Add(aranzman.Smestaj, br);
                                brJed.Add(new Tuple<int, int>(smestaji.IndexOf(aranzman.Smestaj), br));
                            }
                        }
                    }
                }

                if (type.Equals("a"))
                    brJed = brJed.OrderBy(t => t.Item2).ToList();
                else
                    brJed = brJed.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in brJed)
                {
                    ret.Add(smestaji[item.Item1]);
                }
            }

            TempData["smestaji"] = true;
            HttpContext.Application["smestaji"] = ret;
            return RedirectToAction("Smestaji");
        }

        [HttpPost]
        public ActionResult Search(string pocetakMin, string pocetakMax, string zavrsetakMin, string zavrsetakMax, string prevoz, string tipAranzmana, string naziv)
        {

            Regex patern1 = new Regex(@"." + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern2 = new Regex(@"" + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern3 = new Regex(@"." + naziv, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            List<Aranzman> aranzmaniManagera = HttpContext.Application["aranzmaniManagera"] as List<Aranzman>;
            List<Aranzman> ret = new List<Aranzman>();

            DateTime pocMin = new DateTime();
            DateTime pocMax = new DateTime();
            DateTime zavMin = new DateTime();
            DateTime zavMax = new DateTime();

            if (pocetakMin != "")
                pocMin = Convert.ToDateTime(pocetakMin);
            if (pocetakMax != "")
                pocMax = Convert.ToDateTime(pocetakMax);
            if (zavrsetakMin != "")
                zavMin = Convert.ToDateTime(zavrsetakMin);
            if (zavrsetakMax != "")
                zavMax = Convert.ToDateTime(zavrsetakMax);

            foreach (var aranzman in aranzmaniManagera)
            {
                if (pocetakMin != "")
                    if (aranzman.DatumPocetka < pocMin)
                        continue;

                if (pocetakMax != "")
                    if (aranzman.DatumPocetka > pocMax)
                        continue;

                if (zavrsetakMin != "")
                    if (aranzman.DatumZavrsetka < zavMin)
                        continue;
                if (zavrsetakMax != "")
                    if (aranzman.DatumZavrsetka > zavMax)
                        continue;

                if (prevoz != "")
                    if (!aranzman.Prevoz.ToString().Equals(prevoz))
                        continue;

                if (tipAranzmana != "")
                    if (!aranzman.Tip.ToString().Equals(tipAranzmana))
                        continue;

                if (naziv != "")
                    if (!(patern1.Match(aranzman.Naziv).Success ||
                        patern2.Match(aranzman.Naziv).Success ||
                        patern3.Match(aranzman.Naziv).Success))
                        continue;

                ret.Add(aranzman);
            }

            HttpContext.Application["aranzmaniManagera"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Clear(string clear)
        {
            User user = Session["user"] as User;
            List<Aranzman> aranzmaniManagera = user.ListaAranzmana;
            HttpContext.Application["aranzmaniManagera"] = aranzmaniManagera;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Sort(string sort, string type)
        {
            List<Aranzman> aranzmani = HttpContext.Application["aranzmaniManagera"] as List<Aranzman>;
            List<Aranzman> ret = new List<Aranzman>(aranzmani.Count);

            if (sort == "naziv")
            {
                List<string> temp = new List<string>(aranzmani.Count);
                foreach (Aranzman aranzman in aranzmani)
                {
                    temp.Add(aranzman.Naziv);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();

                Aranzman a = new Aranzman();
                foreach (string item in temp)
                {
                    foreach (Aranzman aranzman in aranzmani)
                    {
                        if (aranzman.Naziv.Equals(item))
                        {
                            ret.Add(aranzman);
                            a = aranzman;
                        }
                    }
                    aranzmani.Remove(a);
                }
            }

            if (sort == "datumpocetka")
            {
                List<DateTime> temp = new List<DateTime>(aranzmani.Count);
                foreach (Aranzman aranzman in aranzmani)
                {
                    temp.Add(aranzman.DatumPocetka);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();

                Aranzman a = new Aranzman();
                foreach (DateTime item in temp)
                {
                    foreach (Aranzman aranzman in aranzmani)
                    {
                        if (aranzman.DatumPocetka.Equals(item))
                        {
                            ret.Add(aranzman);
                            a = aranzman;
                        }
                    }
                    aranzmani.Remove(a);
                }
            }

            if (sort == "datumzavrsetka")
            {
                List<DateTime> temp = new List<DateTime>(aranzmani.Count);
                foreach (Aranzman aranzman in aranzmani)
                {
                    temp.Add(aranzman.DatumZavrsetka);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();

                Aranzman a = new Aranzman();
                foreach (DateTime item in temp)
                {
                    foreach (Aranzman aranzman in aranzmani)
                    {
                        if (aranzman.DatumZavrsetka.Equals(item))
                        {
                            ret.Add(aranzman);
                            a = aranzman;
                        }
                    }
                    aranzmani.Remove(a);
                }
            }

            HttpContext.Application["aranzmaniManagera"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SearchRezervacije(string naziv, string pocOd, string pocDo, string tip, string brOd, string brDo, string cenaOd, string cenaDo)
        {
            Regex patern1 = new Regex(@"." + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern2 = new Regex(@"" + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern3 = new Regex(@"." + naziv, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            DateTime pocOdDate = new DateTime();
            if (pocOd != "")
                pocOdDate = Convert.ToDateTime(pocOd);
            DateTime pocDoDate = new DateTime();
            if (pocDo != "")
                pocDoDate = Convert.ToDateTime(pocDo);
            int brOdInt = -1;
            if (brOd != "")
                brOdInt = Convert.ToInt32(brOd);
            int brDoInt = -1;
            if (brDo != "")
                brDoInt = Convert.ToInt32(brDo);
            int cenaOdInt = -1;
            if (cenaOd != "")
                cenaOdInt = Convert.ToInt32(cenaOd);
            int cenaDoInt = -1;
            if (cenaDo != "")
                cenaDoInt = Convert.ToInt32(cenaDo);

            List<Rezervacija> rezervacije = HttpContext.Application["rezervacije"] as List<Rezervacija>;

            List<Rezervacija> ret = new List<Rezervacija>();
            foreach (var rezervacija in rezervacije)
            {
                if (naziv != "")
                {
                    if (!(patern1.Match(rezervacija.Aranzman.Naziv).Success ||
                       patern2.Match(rezervacija.Aranzman.Naziv).Success ||
                       patern3.Match(rezervacija.Aranzman.Naziv).Success))
                        continue;
                }

                if (tip != "")
                    if (!rezervacija.Aranzman.Smestaj.Tip.ToString().Equals(tip))
                        continue;

                if (rezervacija.Aranzman.DatumPocetka.Date < pocOdDate.Date && pocOd != "")
                    continue;
                if (rezervacija.Aranzman.DatumZavrsetka.Date > pocDoDate.Date && pocDo != "")
                    continue;

                if (rezervacija.Smestaj.MaxGostiju < brOdInt && brOdInt != -1)
                    continue;
                if (rezervacija.Smestaj.MaxGostiju > brDoInt && brDoInt != -1)
                    continue;

                if ((rezervacija.Aranzman.DatumZavrsetka - rezervacija.Aranzman.DatumPocetka).TotalDays * rezervacija.Smestaj.Cena < cenaOdInt && cenaOdInt != -1)
                    continue;
                if ((rezervacija.Aranzman.DatumZavrsetka - rezervacija.Aranzman.DatumPocetka).TotalDays * rezervacija.Smestaj.Cena > cenaDoInt && cenaDoInt != -1)
                    continue;

                ret.Add(rezervacija);
            }

            TempData["rezervacije"] = true;
            HttpContext.Application["rezervacije"] = ret;
            return RedirectToAction("Rezervacije");
        }

        [HttpPost]
        public ActionResult ClearRezervacije()
        {
            HttpContext.Application["rezervacije"] = DB.Load<Rezervacija>("~/App_Data/rezervacije.txt");
            return RedirectToAction("Rezervacije");
        }

        [HttpPost]
        public ActionResult SortRezervacije(string selectSort, string type)
        {
            List<Rezervacija> rezervacije = HttpContext.Application["rezervacije"] as List<Rezervacija>;
            List<Rezervacija> ret = new List<Rezervacija>();

            if (selectSort.Equals("hron"))
            {
                User user = Session["user"] as User;

                if (type == "d")
                {
                    for (int i = user.ListaRezervacija.Count - 1; i >= 0; i--)
                    {
                        ret.Add(user.ListaRezervacija[i]);
                    }
                }
                else
                    ret = user.ListaRezervacija;
            }
            else if (selectSort.Equals("name"))
            {
                List<Tuple<int, string>> temp = new List<Tuple<int, string>>();
                int index = 0;
                foreach (var item in rezervacije)
                {
                    temp.Add(new Tuple<int, string>(index++, item.Aranzman.Naziv));
                }

                if (type == "a")
                    temp = temp.OrderBy(t => t.Item2).ToList();
                else
                    temp = temp.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in temp)
                {
                    ret.Add(rezervacije[item.Item1]);
                }

            }
            else if (selectSort.Equals("date"))
            {
                List<Tuple<int, DateTime>> temp = new List<Tuple<int, DateTime>>();
                int index = 0;
                foreach (var item in rezervacije)
                {
                    temp.Add(new Tuple<int, DateTime>(index++, item.Aranzman.DatumPocetka));
                }

                if (type == "a")
                    temp = temp.OrderBy(t => t.Item2).ToList();
                else
                    temp = temp.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in temp)
                {
                    ret.Add(rezervacije[item.Item1]);
                }
            }
            else
            {
                List<Tuple<int, float>> temp = new List<Tuple<int, float>>();
                int index = 0;
                foreach (var item in rezervacije)
                {
                    temp.Add(new Tuple<int, float>(index++, ((float)(item.Aranzman.DatumZavrsetka - item.Aranzman.DatumPocetka).TotalDays) * item.Smestaj.Cena));
                }

                if (type == "a")
                    temp = temp.OrderBy(t => t.Item2).ToList();
                else
                    temp = temp.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in temp)
                {
                    ret.Add(rezervacije[item.Item1]);
                }
            }

            TempData["rezervacije"] = true;
            HttpContext.Application["rezervacije"] = ret;
            return RedirectToAction("Rezervacije");
        }

        [HttpPost]
        public ActionResult Rezervacija(string idRezervacije)
        {
            TempData["id"] = idRezervacije;
            return RedirectToAction("Rezervacija");
        }
        
        [HttpPost]
        public ActionResult PrihvatiKom(int idKom)
        {
            User user = Session["user"] as User;
            List<Komentar> komentari = HttpContext.Application["komentari"] as List<Komentar>;

            komentari[idKom].Status = StatusKomentara.ACCEPTED;
            DB.Save<Komentar>(komentari);
            HttpContext.Application["komentari"] = komentari;

            return RedirectToAction("Komentari", "Home");
        }

        [HttpPost]
        public ActionResult OdbijKom(int idKom)
        {
            User user = Session["user"] as User;
            List<Komentar> komentari = HttpContext.Application["komentari"] as List<Komentar>;

            komentari[idKom].Status = StatusKomentara.DECLINED;
            DB.Save<Komentar>(komentari);
            HttpContext.Application["komentari"] = komentari;

            return RedirectToAction("Komentari", "Home");
        }
    }
}