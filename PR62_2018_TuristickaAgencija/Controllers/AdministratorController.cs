﻿using PR62_2018_TuristickaAgencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_TuristickaAgencija.Controllers
{
    public class AdministratorController : Controller
    {
        // GET: Administrator
        public ActionResult Index()
        {
            User user = Session["user"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.ADMINISTRATOR)
                return RedirectToAction("Index", "Home");

            List<User> users = HttpContext.Application["users"] as List<User>;

            return View(users);
        }

        public ActionResult SumnjiviTuristi()
        {
            User user = Session["user"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.ADMINISTRATOR)
                return RedirectToAction("Index", "Home");

            List<User> users = DB.Load<User>("~/App_Data/users.txt");
            List<User> ret = new List<User>();

            int br;
            foreach (var u in users)
            {
                if(u.Uloga == Uloga.TURISTA && u.Active)
                {
                    br = 0;
                    foreach (var rezervacija in u.ListaRezervacija)
                    {
                        if (rezervacija.Status == Status.OTKAZANA)
                            br++;
                    }
                    if (br >= 2)
                        ret.Add(u);
                }
            }

            return View(ret);
        }

        public ActionResult NoviManager()
        {
            User user = Session["user"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.Uloga != Uloga.ADMINISTRATOR)
                return RedirectToAction("Index", "Home");

            User newMenager = new User();
            ViewBag.Message = TempData["Message"] ?? "Popunite sva polja oznacena *";
            if (TempData["user"] != null)
                newMenager = TempData["user"] as User;

            return View(newMenager);
        }

        [HttpPost]
        public ActionResult Dodaj(User newMenager)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;
            List<User> savedUsers = DB.Load<User>("~/App_Data/users.txt");

            if (savedUsers.Find(u => u.Username.Equals(newMenager.Username)) != null)
            {
                TempData["Message"] = "Korisnik sa ovim korisnickim imenom vec postoji";
                TempData["user"] = newMenager;
                return RedirectToAction("NoviManager");
            }

            newMenager.Active = true;
            newMenager.Uloga = Uloga.MENADZER;
            newMenager.CreateList(newMenager.Uloga);
            users.Add(newMenager);
            savedUsers.Add(newMenager);

            HttpContext.Application["users"] = users;
            DB.Save<User>(users);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Odustani()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Clear()
        {
            HttpContext.Application["users"] = DB.Load<User>("~/App_Data/users.txt");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Search(string firstName, string lastName, string uloga)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;
            List<User> ret = new List<User>();

            foreach (var user in users)
            {
                if (firstName != "" && !user.FirstName.Equals(firstName))
                    continue;
                if (lastName != "" && !user.LastName.Equals(lastName))
                    continue;
                if (uloga != "" && !user.Uloga.ToString().Equals(uloga))
                    continue;

                ret.Add(user);
            }

            HttpContext.Application["users"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Sort(string polje, string type)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;
            List<User> ret = new List<User>();

            if(polje.Equals("firstName"))
            {
                List<Tuple<int, string>> names = new List<Tuple<int, string>>();
                int br = 0;
                foreach (var user in users)
                {
                    names.Add(new Tuple<int, string>(br++, user.FirstName));
                }

                if (type.Equals("a"))
                    names = names.OrderBy(t => t.Item2).ToList();
                else
                    names = names.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in names)
                {
                    ret.Add(users[item.Item1]);
                }

            }
            else if (polje.Equals("lastName"))
            {
                List<Tuple<int, string>> names = new List<Tuple<int, string>>();
                int br = 0;
                foreach (var user in users)
                {
                    names.Add(new Tuple<int, string>(br++, user.LastName));
                }

                if (type.Equals("a"))
                    names = names.OrderBy(t => t.Item2).ToList();
                else
                    names = names.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in names)
                {
                    ret.Add(users[item.Item1]);
                }
            }
            else
            {
                List<Tuple<int, string>> uloge = new List<Tuple<int, string>>();
                int br = 0;
                foreach (var user in users)
                {
                    uloge.Add(new Tuple<int, string>(br++, user.Uloga.ToString()));
                }

                if (type.Equals("a"))
                    uloge = uloge.OrderBy(t => t.Item2).ToList();
                else
                    uloge = uloge.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in uloge)
                {
                    ret.Add(users[item.Item1]);
                }
            }

            HttpContext.Application["users"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Blokiraj(string username)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;
            List<User> usersDB = DB.Load<User>("~/App_Data/users.txt");
            User u = null;

            foreach (var user in usersDB)
            {
                if(user.Username.Equals(username))
                {
                    user.Active = false;
                    break;
                }

            }
            foreach (var user in users)
            {
                if (user.Username.Equals(username))
                {
                    user.Active = false;
                    break;
                }
            }

            HttpContext.Application["users"] = users;
            DB.Save<User>(usersDB);

            return RedirectToAction("SumnjiviTuristi");
        }
    }
}