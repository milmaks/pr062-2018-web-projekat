﻿using PR62_2018_TuristickaAgencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_TuristickaAgencija.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            User user = Session["user"] as User;

            if (user == null)
            {
                user = new User();
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
            }

            ViewBag.User = user;
            ViewBag.Aranzmani = TempData["aranzmani"] ?? "buduci";
            ViewBag.ErrMsg = TempData["errmsg"] ?? string.Empty;
            ViewBag.Message = TempData["Message"] ?? string.Empty;

            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            if (aranzmani.Count > 0 && TempData["sorted"] == null)
            {
                List<Aranzman> temp = aranzmani;
                List<Aranzman> show = new List<Aranzman>(aranzmani.Count);
                Aranzman min = aranzmani[0];
                int i;
                while (aranzmani.Count != 0)
                {
                    for (i = 0; i < aranzmani.Count; i++)
                    {
                        if ((DateTime.Now - min.DatumPocetka).TotalDays < (DateTime.Now - aranzmani[i].DatumPocetka).TotalDays)
                        {
                            min = aranzmani[i];
                            break;
                        }
                    }

                    if (i == aranzmani.Count)
                    {
                        show.Add(min);
                        aranzmani.Remove(min);
                        if (aranzmani.Count > 0)
                            min = aranzmani[0];
                    }

                }

                HttpContext.Application["aranzmani"] = show;
                return View(show);
            }
            return View(aranzmani);
        }

        public ActionResult Komentari()
        {
            User user = Session["user"] as User;

            if (user == null)
            {
                user = new User();
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
            }

            List<Komentar> komentari = HttpContext.Application["komentari"] as List<Komentar>;
            ViewBag.User = user;

            return View(komentari);
        }

        public ActionResult SmestajneJed()
        {
            User user = Session["user"] as User;

            if (user == null)
            {
                user = new User();
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
            }

            List<Smestaj> smestaji = HttpContext.Application["smestajnejed"] as List<Smestaj>;
            ViewBag.User = user;
            ViewBag.Smestaji = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            if (user.Uloga == Uloga.MENADZER)
            {
                ViewBag.Brisanje = "";
                int br = 0;
                foreach (var aranzman in aranzmani)
                {
                    if (aranzman.Active)
                    {
                        if (DateTime.Now < aranzman.DatumPocetka || (DateTime.Now >= aranzman.DatumPocetka && DateTime.Now <= aranzman.DatumZavrsetka))
                        {
                            br = 0;
                            foreach (var jedinica in aranzman.Smestaj.SmestajneJedinice)
                            {
                                if (jedinica.Reserverd && jedinica.Active)
                                {
                                    ViewBag.Brisanje += smestaji.IndexOf(aranzman.Smestaj).ToString();
                                    ViewBag.Brisanje += "|";
                                    ViewBag.Brisanje += br.ToString();
                                    ViewBag.Brisanje += ",";
                                }
                                br++;
                            }
                        }
                    }
                }
            }

            return View(smestaji);
        }

        public ActionResult Rezervacije()
        {
            User user = Session["user"] as User;

            if (user == null)
            {
                user = new User();
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
                return RedirectToAction("Index");
            }

            if(user.Uloga == Uloga.GUEST)
            {
                return RedirectToAction("Index");
            }

            bool change;
            ViewBag.Komentarisane = user.Komentarisane;

            if (TempData["rezervacijeUser"] != null)
            {
                List<Rezervacija> rezervacijeUser = HttpContext.Application["rezervacijeUser"] as List<Rezervacija>;
                change = false;
                foreach (var item in rezervacijeUser)
                {
                    if (DateTime.Now > item.Aranzman.DatumZavrsetka && item.Status == Status.AKTIVNA)
                    {
                        item.Status = Status.ZAVRSENA;
                        change = true;
                    }
                }

                if (change)
                {
                    List<User> users = HttpContext.Application["users"] as List<User>;
                    int indexUser = users.IndexOf(user);
                    users[indexUser] = user;
                    DB.Save<User>(users);
                    Session["user"] = user;
                }

                ViewBag.Change = true;
                return View(rezervacijeUser);
            }

            change = false;
            foreach (var item in user.ListaRezervacija)
            {
                if (DateTime.Now > item.Aranzman.DatumZavrsetka && item.Status == Status.AKTIVNA)
                {
                    item.Status = Status.ZAVRSENA;
                    change = true;
                }
            }

            if(change)
            {
                List<User> users = HttpContext.Application["users"] as List<User>;
                int indexUser = users.IndexOf(user);
                users[indexUser] = user;
                DB.Save<User>(users);
                Session["user"] = user;
            }

            HttpContext.Application["rezervacijeUser"] = user.ListaRezervacija;
            return View(user.ListaRezervacija);
        }

        [HttpPost]
        public ActionResult SearchSmestajneJedinice(string donjaGranica, string gornjaGranica, string ljubimci, string cenaOd, string cenaDo)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestajnejed"] as List<Smestaj>;

            int donjaGranicaBr = -1;
            if (donjaGranica != "")
                donjaGranicaBr = Convert.ToInt32(donjaGranica);
            int gornjaGranicaBr = -1;
            if (gornjaGranica != "")
                gornjaGranicaBr = Convert.ToInt32(gornjaGranica);
            int cenaOdBr = -1;
            if(cenaOd != "")
                cenaOdBr = Convert.ToInt32(cenaOd);
            int cenaDoBr = -1;
            if (cenaDo != "")
                cenaDoBr = Convert.ToInt32(cenaDo);

            foreach (Smestaj smestaj in smestaji)
            {
                foreach (SmestajnaJedinica jedinica in smestaj.SmestajneJedinice)
                {
                    if (jedinica.MaxGostiju < donjaGranicaBr && donjaGranicaBr != -1)
                    {
                        jedinica.Active = false;
                        continue;
                    }

                    if (jedinica.MaxGostiju > gornjaGranicaBr && gornjaGranicaBr != -1)
                    {
                        jedinica.Active = false;
                        continue;
                    }

                    if (ljubimci != null)
                    {
                        if (ljubimci.Equals("Da"))
                        {
                            if (!jedinica.PetFriendly)
                            {
                                jedinica.Active = false;
                                continue;
                            }
                        }
                        else
                        {
                            if (jedinica.PetFriendly)
                            {
                                jedinica.Active = false;
                                continue;
                            }
                        }
                    }

                    if (jedinica.Cena < cenaOdBr && cenaOdBr != -1)
                    {
                        jedinica.Active = false;
                        continue;
                    }

                    if (jedinica.Cena > cenaDoBr && cenaDoBr != -1)
                    {
                        jedinica.Active = false;
                        continue;
                    }

                }
            }
            
            
            HttpContext.Application["smestajnejed"] = smestaji;
            return RedirectToAction("SmestajneJed");
        }

        [HttpPost]
        public ActionResult ClearSmestajneJed()
        {
            List<Smestaj> smestaji = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            HttpContext.Application["smestajnejed"] = smestaji;
            return RedirectToAction("SmestajneJed");
        }

        [HttpPost]
        public ActionResult SortSmestajneJed(string sort)
        {
            List<Smestaj> smestaji = HttpContext.Application["smestajnejed"] as List<Smestaj>;

            if (sort.Substring(0,4) == "cena")
            {
                List<Tuple<int, float>> cene = new List<Tuple<int, float>>();
                List<SmestajnaJedinica> jed = new List<SmestajnaJedinica>();
                int br;
                foreach (var smestaj in smestaji)
                {
                    cene.Clear();
                    jed.Clear();
                    br = 0;
                    foreach (var jedinica in smestaj.SmestajneJedinice)
                    {
                        jed.Add(jedinica);
                        cene.Add(new Tuple<int, float>(br++, jedinica.Cena));
                    }

                    if (sort.Substring(sort.Length - 7) == "Rastuce")
                        cene = cene.OrderBy(t => t.Item2).ToList();
                    else
                        cene = cene.OrderByDescending(t => t.Item2).ToList();

                    br = 0;
                    foreach (var item in cene)
                    {
                        smestaj.SmestajneJedinice[br++] = jed[item.Item1];
                    }
                }
            }
            else
            {
                List<Tuple<int, int>> gosti = new List<Tuple<int, int>>();
                List<SmestajnaJedinica> jed = new List<SmestajnaJedinica>();
                int br;
                foreach (var smestaj in smestaji)
                {
                    gosti.Clear();
                    jed.Clear();
                    br = 0;
                    foreach (var jedinica in smestaj.SmestajneJedinice)
                    {
                        jed.Add(jedinica);
                        gosti.Add(new Tuple<int, int>(br++, jedinica.MaxGostiju));
                    }

                    if (sort.Substring(sort.Length - 7) == "Rastuce")
                        gosti = gosti.OrderBy(t => t.Item2).ToList();
                    else
                        gosti = gosti.OrderByDescending(t => t.Item2).ToList();

                    br = 0;
                    foreach (var item in gosti)
                    {
                        smestaj.SmestajneJedinice[br++] = jed[item.Item1];
                    }
                }
            }

            HttpContext.Application["smestajnejed"] = smestaji;
            return RedirectToAction("SmestajneJed");
        }

        [HttpPost]
        public ActionResult PastAranzmani()
        {
            TempData["aranzmani"] = "prosli";
            return RedirectToAction("Index");
        }

        public ActionResult Pregled()
        {
            if (TempData["index"] == null)
            {
                return RedirectToAction("Index");
            }

            Aranzman aranzman;

            User user = Session["user"] as User;

            if (user == null)
            {
                user = new User();
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
            }

            ViewBag.User = user;
            ViewBag.Index = TempData["index"];

            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            aranzman = aranzmani[Convert.ToInt32(TempData["index"])];

            return View(aranzman);
        }

        [HttpPost]
        public ActionResult Pregled(string index)
        {
            TempData["index"] = index;
            return RedirectToAction("Pregled");
        }

        [HttpPost]
        public ActionResult Search(string pocetakMin, string pocetakMax, string zavrsetakMin, string zavrsetakMax, string prevoz, string tipAranzmana, string naziv, string vremeAranzmana)
        {

            Regex patern1 = new Regex(@"." + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern2 = new Regex(@"" + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern3 = new Regex(@"." + naziv, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            List<Aranzman> ret = new List<Aranzman>();

            DateTime pocMin = new DateTime();
            DateTime pocMax = new DateTime();
            DateTime zavMin = new DateTime();
            DateTime zavMax = new DateTime();

            if(pocetakMin != "")
                pocMin = Convert.ToDateTime(pocetakMin);
            if(pocetakMax != "")
                pocMax = Convert.ToDateTime(pocetakMax);
            if(zavrsetakMin != "")
                zavMin = Convert.ToDateTime(zavrsetakMin);
            if(zavrsetakMax != "")
                zavMax = Convert.ToDateTime(zavrsetakMax);

            if (vremeAranzmana.Equals("buduci"))
            {
                if(pocetakMin != "")
                {
                    if (pocMin < DateTime.Today)
                    {
                        TempData["errmsg"] = "Za pregled proslih aranzmana predjite na karticu: Prosli aranzmani";
                        return RedirectToAction("Index");
                    }
                }

                if(zavrsetakMin != "")
                {
                    if (zavMin < DateTime.Today)
                    {
                        TempData["errmsg"] = "Za pregled proslih aranzmana predjite na karticu: Prosli aranzmani";
                        return RedirectToAction("Index");
                    }
                }

                if(zavrsetakMax != "")
                {
                    if (zavMax < DateTime.Today)
                    {
                        TempData["errmsg"] = "Za pregled proslih aranzmana predjite na karticu: Prosli aranzmani";
                        return RedirectToAction("Index");
                    }
                }   
            }

            foreach (var aranzman in aranzmani)
            {
                if (pocetakMin != "")
                    if (aranzman.DatumPocetka < pocMin)
                        continue;

                if(pocetakMax != "")
                    if (aranzman.DatumPocetka > pocMax)
                        continue;

                if(zavrsetakMin != "")
                    if (aranzman.DatumZavrsetka < zavMin)
                        continue;
                if(zavrsetakMax != "")
                    if (aranzman.DatumZavrsetka > zavMax)
                        continue;

                if(prevoz != "")
                    if (!aranzman.Prevoz.ToString().Equals(prevoz))
                        continue;

                if(tipAranzmana != "")
                    if (!aranzman.Tip.ToString().Equals(tipAranzmana))
                        continue;

                if(naziv != "")
                    if (!(patern1.Match(aranzman.Naziv).Success ||
                        patern2.Match(aranzman.Naziv).Success ||
                        patern3.Match(aranzman.Naziv).Success))
                        continue;

                ret.Add(aranzman);
            }

            TempData["aranzmani"] = vremeAranzmana;
            HttpContext.Application["aranzmani"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Clear(string clear)
        {
            TempData["aranzmani"] = clear;
            List<Aranzman> aranzmani = DB.Load<Aranzman>("~/App_Data/aranzmani.txt");
            HttpContext.Application["aranzmani"] = aranzmani;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Sort(string sort, string type, string vremeAranzmana)
        {
            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            List<Aranzman> ret = new List<Aranzman>(aranzmani.Count);

            if (sort == "naziv")
            {
                List<string> temp = new List<string>(aranzmani.Count);
                foreach (Aranzman aranzman in aranzmani)
                {
                    temp.Add(aranzman.Naziv);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();

                Aranzman a = new Aranzman();
                foreach (string item in temp)
                {
                    foreach (Aranzman aranzman in aranzmani)
                    {
                        if(aranzman.Naziv.Equals(item))
                        {
                            ret.Add(aranzman);
                            a = aranzman;
                        }
                    }
                    aranzmani.Remove(a);
                }
            }

            if (sort == "datumpocetka")
            {
                List<DateTime> temp = new List<DateTime>(aranzmani.Count);
                foreach (Aranzman aranzman in aranzmani)
                {
                    temp.Add(aranzman.DatumPocetka);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();

                Aranzman a = new Aranzman();
                foreach (DateTime item in temp)
                {
                    foreach (Aranzman aranzman in aranzmani)
                    {
                        if (aranzman.DatumPocetka.Equals(item))
                        {
                            ret.Add(aranzman);
                            a = aranzman;
                        }
                    }
                    aranzmani.Remove(a);
                }
            }

            if (sort == "datumzavrsetka")
            {
                List<DateTime> temp = new List<DateTime>(aranzmani.Count);
                foreach (Aranzman aranzman in aranzmani)
                {
                    temp.Add(aranzman.DatumZavrsetka);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();

                Aranzman a = new Aranzman();
                foreach (DateTime item in temp)
                {
                    foreach (Aranzman aranzman in aranzmani)
                    {
                        if (aranzman.DatumZavrsetka.Equals(item))
                        {
                            ret.Add(aranzman);
                            a = aranzman;
                        }
                    }
                    aranzmani.Remove(a);
                }
            }

            TempData["sorted"] = true;
            TempData["aranzmani"] = vremeAranzmana;
            HttpContext.Application["aranzmani"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            User user = Session["user"] as User;

            if (user == null)
            {
                user = new User();
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
            }

            List<User> users = HttpContext.Application["users"] as List<User>;
            User newUser = users.Find(u => u.Username.Equals(username) && u.Password.Equals(password));

            if (newUser == null)
            {
                TempData["Message"] = "Ne postoji korisnik sa ovim korisnickim imenom i lozinkom";
                return RedirectToAction("Index", "Home");
            }

            if (newUser.Active == false)
            {
                TempData["Message"] = "Vas nalog je blokiran zbog broja otkazanih rezervacija, kontaktirajte administratore";
                return RedirectToAction("Index", "Home");
            }

            Session["user"] = newUser;
            if (newUser.Uloga == Uloga.TURISTA)
                HttpContext.Application["rezervacijeUser"] = newUser.ListaRezervacija;
            if (newUser.Uloga == Uloga.MENADZER)
                HttpContext.Application["aranzmaniManagera"] = newUser.ListaAranzmana;

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult ProfileChange()
        {
            return RedirectToAction("Profile", "Authentication");
        }

        [HttpPost]
        public ActionResult Rezervisi(string indexSmestajJed, string indexAranzmana)
        {
            int indexA = Convert.ToInt32(indexAranzmana);
            int indexJ = Convert.ToInt32(indexSmestajJed);

            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            List<Rezervacija> rezervacijeUser = HttpContext.Application["rezervacijeUser"] as List<Rezervacija>;
            List<Rezervacija> rezervacije = HttpContext.Application["rezervacije"] as List<Rezervacija>;
            List<User> users = HttpContext.Application["users"] as List<User>;

            aranzmani[indexA].Smestaj.SmestajneJedinice[indexJ].Reserverd = true;
            foreach (var item in users)
            {
                if (item.Uloga == Uloga.MENADZER)
                    if (item.ListaAranzmana.Contains(aranzmani[indexA]))
                        item.ListaAranzmana[item.ListaAranzmana.IndexOf(aranzmani[indexA])].Smestaj.SmestajneJedinice[indexJ].Reserverd = true;
            }
            DB.Save<Aranzman>(aranzmani);

            User user = Session["user"] as User;
            int indexUser = users.IndexOf(user);
            Rezervacija rezervacija = new Rezervacija(generateID(), user, Status.AKTIVNA, aranzmani[indexA], aranzmani[indexA].Smestaj.SmestajneJedinice[indexJ]);
            Rezervacija rezervacijaUser = new Rezervacija(rezervacija.Id, null, Status.AKTIVNA, aranzmani[indexA], aranzmani[indexA].Smestaj.SmestajneJedinice[indexJ]);
            users[indexUser].ListaRezervacija.Add(rezervacijaUser);
            rezervacije.Add(rezervacija);
            DB.Save<User>(users);
            DB.Save<Rezervacija>(rezervacije);
            HttpContext.Application["users"] = users;
            Session["user"] = users[indexUser];

            HttpContext.Application["aranzmani"] = aranzmani;
            HttpContext.Application["rezervacijeUser"] = rezervacijeUser;
            HttpContext.Application["rezervacije"] = rezervacije;

            TempData["index"] = indexAranzmana;
            return RedirectToAction("Pregled", "Home");
        }

        [HttpPost]
        public ActionResult Otkazi(string indexRez)
        {
            int index = Convert.ToInt32(indexRez);

            User user = Session["user"] as User;
            List<Aranzman> aranzmani = HttpContext.Application["aranzmani"] as List<Aranzman>;
            List<Rezervacija> rezervacijeUser = HttpContext.Application["rezervacijeUser"] as List<Rezervacija>;
            List<Rezervacija> rezervacije = HttpContext.Application["rezervacije"] as List<Rezervacija>;
            List<User> users = HttpContext.Application["users"] as List<User>;
            int indexUser = users.IndexOf(user);
            string idRez = rezervacijeUser[index].Id;

            index = 0;
            foreach (var rez in user.ListaRezervacija)
            {
                if (rez.Id.Equals(idRez))
                    break;
                index++;
            }

            int indexA = aranzmani.IndexOf(user.ListaRezervacija[index].Aranzman);
            int indexJ = aranzmani[indexA].Smestaj.SmestajneJedinice.IndexOf(users[indexUser].ListaRezervacija[index].Smestaj);


            foreach (var u in users)
            {
                if(u.Uloga == Uloga.MENADZER)
                {
                    if(u.ListaAranzmana.Contains(aranzmani[indexA]))
                    {
                        foreach (var item in u.ListaAranzmana)
                        {
                            if (item.Equals(aranzmani[indexA]))
                                item.Smestaj.SmestajneJedinice[indexJ].Reserverd = false;
                        }
                    }
                }
            }
            aranzmani[indexA].Smestaj.SmestajneJedinice[indexJ].Reserverd = false;
            users[indexUser].ListaRezervacija[index].Status = Status.OTKAZANA;
            foreach (var rez in rezervacije)
            {
                if (rez.Id.Equals(idRez))
                {
                    rez.Status = Status.OTKAZANA;
                    break;
                }
            }

            DB.Save<User>(users);
            DB.Save<Aranzman>(aranzmani);
            DB.Save<Rezervacija>(rezervacije);

            HttpContext.Application["users"] = users;
            Session["user"] = users[indexUser];

            HttpContext.Application["aranzmani"] = aranzmani;
            HttpContext.Application["rezervacije"] = rezervacije;

            return RedirectToAction("Rezervacije");
        }

        [HttpPost]
        public ActionResult SearchRezervacije(string naziv, string pocOd, string pocDo, string tip, string brOd, string brDo, string cenaOd, string cenaDo)
        {
            Regex patern1 = new Regex(@"." + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern2 = new Regex(@"" + naziv + ".", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex patern3 = new Regex(@"." + naziv, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            DateTime pocOdDate = new DateTime();
            if (pocOd != "")
                pocOdDate = Convert.ToDateTime(pocOd);
            DateTime pocDoDate = new DateTime();
            if (pocDo != "")
                pocDoDate = Convert.ToDateTime(pocDo);
            int brOdInt = -1;
            if (brOd != "")
                brOdInt = Convert.ToInt32(brOd);
            int brDoInt = -1;
            if (brDo != "")
                brDoInt = Convert.ToInt32(brDo);
            int cenaOdInt = -1;
            if (cenaOd != "")
                cenaOdInt = Convert.ToInt32(cenaOd);
            int cenaDoInt = -1;
            if (cenaDo != "")
                cenaDoInt = Convert.ToInt32(cenaDo);

            List<Rezervacija> rezervacijeUser = HttpContext.Application["rezervacijeUser"] as List<Rezervacija>;

            List<Rezervacija> ret = new List<Rezervacija>();
            foreach (var rezervacija in rezervacijeUser)
            {
                if (naziv != "")
                {
                    if (!(patern1.Match(rezervacija.Aranzman.Naziv).Success ||
                       patern2.Match(rezervacija.Aranzman.Naziv).Success ||
                       patern3.Match(rezervacija.Aranzman.Naziv).Success))
                        continue;
                }

                if (tip != "")
                    if(!rezervacija.Aranzman.Smestaj.Tip.ToString().Equals(tip))
                        continue;

                if (rezervacija.Aranzman.DatumPocetka.Date < pocOdDate.Date && pocOd != "")
                    continue;
                if (rezervacija.Aranzman.DatumZavrsetka.Date > pocDoDate.Date && pocDo != "")
                    continue;

                if (rezervacija.Smestaj.MaxGostiju < brOdInt && brOdInt != -1)
                    continue;
                if (rezervacija.Smestaj.MaxGostiju > brDoInt && brDoInt != -1)
                    continue;

                if ((rezervacija.Aranzman.DatumZavrsetka - rezervacija.Aranzman.DatumPocetka).TotalDays * rezervacija.Smestaj.Cena < cenaOdInt && cenaOdInt != -1)
                    continue;
                if ((rezervacija.Aranzman.DatumZavrsetka - rezervacija.Aranzman.DatumPocetka).TotalDays * rezervacija.Smestaj.Cena > cenaDoInt && cenaDoInt != -1)
                    continue;

                ret.Add(rezervacija);
            }

            TempData["rezervacijeUser"] = true;
            HttpContext.Application["rezervacijeUser"] = ret;
            return RedirectToAction("Rezervacije");
        }

        [HttpPost]
        public ActionResult ClearRezervacije()
        {
            return RedirectToAction("Rezervacije");
        }

        [HttpPost]
        public ActionResult SortRezervacije(string selectSort, string type)
        {
            List<Rezervacija> rezervacijeUser = HttpContext.Application["rezervacijeUser"] as List<Rezervacija>;
            List<Rezervacija> ret = new List<Rezervacija>();
            
            if(selectSort.Equals("hron"))
            {
                User user = Session["user"] as User;

                if (type == "d")
                {
                    for (int i = user.ListaRezervacija.Count - 1; i >= 0; i--)
                    {
                        ret.Add(user.ListaRezervacija[i]);
                    }
                }
                else
                    ret = user.ListaRezervacija;
            }
            else if(selectSort.Equals("name"))
            {
                List<Tuple<int, string>> temp = new List<Tuple<int, string>>();
                int index = 0;
                foreach (var item in rezervacijeUser)
                {
                    temp.Add(new Tuple<int, string>(index++, item.Aranzman.Naziv));
                }

                if (type == "a")
                    temp = temp.OrderBy(t => t.Item2).ToList();
                else
                    temp = temp.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in temp)
                {
                    ret.Add(rezervacijeUser[item.Item1]);
                }

            }
            else if (selectSort.Equals("date"))
            {
                List<Tuple<int, DateTime>> temp = new List<Tuple<int, DateTime>>();
                int index = 0;
                foreach (var item in rezervacijeUser)
                {
                    temp.Add(new Tuple<int, DateTime>(index++, item.Aranzman.DatumPocetka));
                }

                if (type == "a")
                    temp = temp.OrderBy(t => t.Item2).ToList();
                else
                    temp = temp.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in temp)
                {
                    ret.Add(rezervacijeUser[item.Item1]);
                }
            }
            else
            {
                List<Tuple<int, float>> temp = new List<Tuple<int, float>>();
                int index = 0;
                foreach (var item in rezervacijeUser)
                {
                    temp.Add(new Tuple<int, float>(index++,((float)(item.Aranzman.DatumZavrsetka- item.Aranzman.DatumPocetka).TotalDays)*item.Smestaj.Cena));
                }

                if (type == "a")
                    temp = temp.OrderBy(t => t.Item2).ToList();
                else
                    temp = temp.OrderByDescending(t => t.Item2).ToList();

                foreach (var item in temp)
                {
                    ret.Add(rezervacijeUser[item.Item1]);
                }
            }

            TempData["rezervacijeUser"] = true;
            HttpContext.Application["rezervacijeUser"] = ret;
            return RedirectToAction("Rezervacije");
        }

        [HttpPost]
        public ActionResult AddSmestajnaJedinica(int idSmestaj)
        {
            TempData["idSmestaj"] = idSmestaj;
            return RedirectToAction("SmestajnaJed", "Manager");
        }

        [HttpPost]
        public ActionResult ChangeSmestajnaJedinica(string indexi)
        {
            TempData["indexi"] = indexi;
            TempData["izmena"] = "izmena";
            return RedirectToAction("SmestajnaJed", "Manager");
        }

        [HttpPost]
        public ActionResult Komentarisi(string idRez, string tekst, float ocena)
        {
            User user = Session["user"] as User;
            List<Rezervacija> rezervacije = HttpContext.Application["rezervacije"] as List<Rezervacija>;
            List<Komentar> komentari = HttpContext.Application["komentari"] as List<Komentar>;
            List<User> users = HttpContext.Application["users"] as List<User>;

            Rezervacija r = null;
            int indexUser = users.IndexOf(user);

            foreach (var rezervacija in rezervacije)
            {
                if (rezervacija.Id.Equals(idRez))
                    r = rezervacija;
            }

            Komentar komentar = new Komentar(r.Turista, r.Aranzman, tekst, ocena, StatusKomentara.PENDING);
            komentari.Add(komentar);
            users[indexUser].Komentarisane.Add(idRez);
            Session["user"] = users[indexUser];

            DB.Save<User>(users);
            DB.Save<Komentar>(komentari);

            return RedirectToAction("Rezervacije");
        }

        private string generateID()
        {
            return Guid.NewGuid().ToString("N").Substring(0,15);
        }
    }
}