﻿using PR62_2018_TuristickaAgencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_TuristickaAgencija.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        public ActionResult Index()
        {
            ViewBag.Message = TempData["Message"] ?? "*Obavezna polja";

            User user = Session["user"] as User;

            if (user == null)
            {
                user = new User();
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
            }

            return View(user);
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;

            if (users.Find(u => u.Username.Equals(user.Username)) != null)
            {
                TempData["Message"] = "Korisnik sa ovim korisnickim imenom vec postoji";
                user.Uloga = Uloga.GUEST;
                Session["user"] = user;
                return RedirectToAction("Index");
            }

            if (user != null)
            {
                user.Active = true;
                user.Uloga = Uloga.TURISTA;
                user.CreateList(user.Uloga);
                users.Add(user);
                HttpContext.Application["users"] = users;
                DB.Save<User>(users);

                Session["user"] = user;
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult ProfileChange()
        {
            List<User> users = HttpContext.Application["users"] as List<User>;

            User user = Session["user"] as User;

            if (user == null)
                return RedirectToAction("Index", "Home");

            if(user.Uloga == Uloga.GUEST)
                return RedirectToAction("Index", "Home");

            ViewBag.Index = users.IndexOf(user).ToString();
            ViewBag.Message = TempData["Msg"] ?? "Da lozinka ostane nepromenjena potrebno je ponoviti je!";

            return View(user);
        }

        [HttpPost]
        public ActionResult ChangeUserInfo(User user, string index, string newPassword)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;

            int i = Convert.ToInt32(index);

            if (user.Password != users[i].Password)
            {
                TempData["Msg"] = "Pogresna lozinka!";
                return RedirectToAction("ProfileChange");
            }

            users[i].Username = user.Username;
            users[i].Password = newPassword;
            users[i].FirstName = user.FirstName;
            users[i].LastName = user.LastName;
            users[i].Pol = user.Pol;
            users[i].Email = user.Email;
            users[i].DatumRodjenja = user.DatumRodjenja;

            TempData["Msg"] = "Izmene uspesno sacuvane";
            DB.Save<User>(users);

            return RedirectToAction("ProfileChange");
        }

        [HttpPost]
        public ActionResult Potvrdi()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut()
        {
            Session["user"] = null;

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Odustani()
        {
            Session["user"] = null;

            return RedirectToAction("Index", "Home");
        }
    }
}