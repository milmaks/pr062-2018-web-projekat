﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR62_2018_TuristickaAgencija.Models
{
    [Serializable]
    public class MestoNalazenja
    {
        private string adresa;
        private string geoSirina;
        private string geoDuzina;

        public string Adresa { get => adresa; set => adresa = value; }
        public string GeoSirina { get => geoSirina; set => geoSirina = value; }
        public string GeoDuzina { get => geoDuzina; set => geoDuzina = value; }

        public MestoNalazenja(string adresa, string geoSirina, string geoDuzina)
        {
            this.Adresa = adresa;
            this.GeoSirina = geoSirina;
            this.GeoDuzina = geoDuzina;
        }

        public MestoNalazenja()
        {
            this.Adresa = string.Empty;
            this.GeoSirina = string.Empty;
            this.GeoDuzina = string.Empty;
        }

        public override bool Equals(object obj)
        {
            var nalazenja = obj as MestoNalazenja;
            return nalazenja != null &&
                   adresa == nalazenja.adresa &&
                   geoSirina == nalazenja.geoSirina &&
                   geoDuzina == nalazenja.geoDuzina;
        }

        public override int GetHashCode()
        {
            var hashCode = 571488104;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(adresa);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(geoSirina);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(geoDuzina);
            return hashCode;
        }
    }
}