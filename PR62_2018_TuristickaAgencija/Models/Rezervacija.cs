﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace PR62_2018_TuristickaAgencija.Models
{
    [Serializable]
    public class Rezervacija
    {
        private string id;
        private User turista;
        private Status status;
        private Aranzman aranzman;
        private SmestajnaJedinica smestaj;

        public User Turista { get => turista; set => turista = value; }
        public Status Status { get => status; set => status = value; }
        public Aranzman Aranzman { get => aranzman; set => aranzman = value; }
        public SmestajnaJedinica Smestaj { get => smestaj; set => smestaj = value; }
        public string Id { get => id; set => id = value; }

        public Rezervacija(string id, User turista, Status status, Aranzman aranzman, SmestajnaJedinica smestaj)
        {
            this.Id = id;
            this.Turista = turista;
            this.Status = status;
            this.Aranzman = aranzman;
            this.Smestaj = smestaj;
        }

        public Rezervacija()
        {
            this.Id = string.Empty;
            this.Turista = null;
            this.Status = Status.NEPOZNATO;
            this.Aranzman = null;
            this.Smestaj = null;
        }
    }
}