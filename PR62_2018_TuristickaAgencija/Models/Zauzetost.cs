﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR62_2018_TuristickaAgencija.Models
{
    [Serializable]
    public class Zauzetost
    {
        private Dictionary<Smestaj, List<Tuple<DateTime, DateTime>>> smestaji;

        public Zauzetost()
        {
            smestaji = new Dictionary<Smestaj, List<Tuple<DateTime, DateTime>>>();
        }

        public Dictionary<Smestaj, List<Tuple<DateTime, DateTime>>> Smestaji { get => smestaji; set => smestaji = value; }
    }
}