﻿using System;

namespace PR62_2018_TuristickaAgencija.Models
{
    [Serializable]
    public class SmestajnaJedinica
    {
        private int maxGostiju;
        private bool petFriendly;
        private float cena;
        private bool reserverd = false;
        private bool active = false;

        public int MaxGostiju { get => maxGostiju; set => maxGostiju = value; }
        public bool PetFriendly { get => petFriendly; set => petFriendly = value; }
        public float Cena { get => cena; set => cena = value; }
        public bool Reserverd { get => reserverd; set => reserverd = value; }
        public bool Active { get => active; set => active = value; }

        public SmestajnaJedinica(int maxGostiju, bool petFriendly, float cena)
        {
            this.MaxGostiju = maxGostiju;
            this.PetFriendly = petFriendly;
            this.Cena = cena;
        }

        public SmestajnaJedinica()
        {
            this.MaxGostiju = 0;
            this.PetFriendly = false;
            this.Cena = 0;
        }

        public override bool Equals(object obj)
        {
            var jedinica = obj as SmestajnaJedinica;
            return jedinica != null &&
                   maxGostiju == jedinica.maxGostiju &&
                   petFriendly == jedinica.petFriendly &&
                   cena == jedinica.cena &&
                   reserverd == jedinica.reserverd;
        }

        public override int GetHashCode()
        {
            var hashCode = -1315783024;
            hashCode = hashCode * -1521134295 + maxGostiju.GetHashCode();
            hashCode = hashCode * -1521134295 + petFriendly.GetHashCode();
            hashCode = hashCode * -1521134295 + cena.GetHashCode();
            hashCode = hashCode * -1521134295 + reserverd.GetHashCode();
            return hashCode;
        }
    }
}