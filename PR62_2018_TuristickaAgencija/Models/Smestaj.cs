﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR62_2018_TuristickaAgencija.Models
{
    [Serializable]
    public class Smestaj
    {
        private TipSmestaja tip;
        private string naziv;
        private float brojZvezdica;
        private bool bazen;
        private bool spa;
        private bool prilagodjen;
        private bool wifi;
        private List<SmestajnaJedinica> smestajneJedinice;
        private bool active = false;

        public TipSmestaja Tip { get => tip; set => tip = value; }
        public string Naziv { get => naziv; set => naziv = value; }
        public float BrojZvezdica { get => brojZvezdica; set => brojZvezdica = value; }
        public bool Bazen { get => bazen; set => bazen = value; }
        public bool Spa { get => spa; set => spa = value; }
        public bool Prilagodjen { get => prilagodjen; set => prilagodjen = value; }
        public bool Wifi { get => wifi; set => wifi = value; }
        public List<SmestajnaJedinica> SmestajneJedinice { get => smestajneJedinice; set => smestajneJedinice = value; }
        public bool Active { get => active; set => active = value; }

        public Smestaj(TipSmestaja tip, string naziv, float brojZvezdica, bool bazen, bool spa, bool prilagodjen, bool wifi, List<SmestajnaJedinica> smestajneJedinice)
        {
            this.Tip = tip;
            this.Naziv = naziv;
            this.BrojZvezdica = brojZvezdica;
            this.Bazen = bazen;
            this.Spa = spa;
            this.Prilagodjen = prilagodjen;
            this.Wifi = wifi;
            this.SmestajneJedinice = smestajneJedinice;
        }

        public Smestaj()
        {
            this.Naziv = string.Empty;
            this.BrojZvezdica = 0;
            this.Bazen = false;
            this.Spa = false;
            this.Prilagodjen = false;
            this.Wifi = false;
            this.SmestajneJedinice = new List<SmestajnaJedinica>();
        }

        public override bool Equals(object obj)
        {
            var smestaj = obj as Smestaj;
            return smestaj != null &&
                   tip == smestaj.tip &&
                   naziv == smestaj.naziv &&
                   brojZvezdica == smestaj.brojZvezdica &&
                   bazen == smestaj.bazen &&
                   spa == smestaj.spa &&
                   prilagodjen == smestaj.prilagodjen &&
                   wifi == smestaj.wifi;
        }

        public override int GetHashCode()
        {
            var hashCode = 1290866947;
            hashCode = hashCode * -1521134295 + tip.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(naziv);
            hashCode = hashCode * -1521134295 + brojZvezdica.GetHashCode();
            hashCode = hashCode * -1521134295 + bazen.GetHashCode();
            hashCode = hashCode * -1521134295 + spa.GetHashCode();
            hashCode = hashCode * -1521134295 + prilagodjen.GetHashCode();
            hashCode = hashCode * -1521134295 + wifi.GetHashCode();
            return hashCode;
        }
    }
}