﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR62_2018_TuristickaAgencija.Models
{
    public enum Status
    {
        AKTIVNA,
        ZAVRSENA,
        OTKAZANA,
        NEPOZNATO
    }
}