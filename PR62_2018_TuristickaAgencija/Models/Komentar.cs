﻿namespace PR62_2018_TuristickaAgencija.Models
{
    public class Komentar
    {
        private User turista;
        private Aranzman aranzman;
        private string tekst;
        private float ocena;
        private StatusKomentara status;

        public User Turista { get => turista; set => turista = value; }
        public Aranzman Aranzman { get => aranzman; set => aranzman = value; }
        public float Ocena { get => ocena; set => ocena = value; }
        public string Tekst { get => tekst; set => tekst = value; }
        public StatusKomentara Status { get => status; set => status = value; }

        public Komentar(User turista, Aranzman aranzman, string komentar, float ocena, StatusKomentara status)
        {
            this.Turista = turista;
            this.Aranzman = aranzman;
            this.Tekst = komentar;
            this.Ocena = ocena;
            this.Status = status;
        }

        public Komentar()
        {
            this.Turista = null;
            this.Aranzman = null;
            this.Tekst = string.Empty;
            this.Ocena = 0;
            this.Status = StatusKomentara.PENDING;
        }
    }
}