﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace PR62_2018_TuristickaAgencija.Models
{
    [Serializable]
    public class Aranzman
    {
        private string naziv;
        private TipAranzmana tip;
        private TipPrevoza prevoz;
        private string lokacija;
        private DateTime datumPocetka;
        private DateTime datumZavrsetka;
        private MestoNalazenja mesto;
        private string vremeNalazenja;
        private int maxPutnika;
        private string opis;
        private string programPutovanja;
        private string path;
        private Smestaj smestaj;
        private bool active = false;

        public string Naziv { get => naziv; set => naziv = value; }
        public TipAranzmana Tip { get => tip; set => tip = value; }
        public TipPrevoza Prevoz { get => prevoz; set => prevoz = value; }
        public string Lokacija { get => lokacija; set => lokacija = value; }
        public MestoNalazenja Mesto { get => mesto; set => mesto = value; }
        public string VremeNalazenja { get => vremeNalazenja; set => vremeNalazenja = value; }
        public int MaxPutnika { get => maxPutnika; set => maxPutnika = value; }
        public string Opis { get => opis; set => opis = value; }
        public string ProgramPutovanja { get => programPutovanja; set => programPutovanja = value; }
        public string Path { get => path; set => path = value; }
        public Smestaj Smestaj { get => smestaj; set => smestaj = value; }

        [XmlIgnore]
        public DateTime DatumPocetka { get => datumPocetka; set => datumPocetka = value; }
        [XmlElement("DatumPocetka")]
        public string SomeDateString
        {
            get { return this.DatumPocetka.ToString("dd/MM/yyyy"); }
            set {
                    var temp = value.Split('/');
                    var temp1 = temp[0];
                    temp[0] = temp[1];
                    temp[1] = temp1;
                    value = temp[0] + "/" + temp[1] + "/" + temp[2];
                    this.DatumPocetka = DateTime.Parse(value);
                }
        }
        [XmlIgnore]
        public DateTime DatumZavrsetka { get => datumZavrsetka; set => datumZavrsetka = value; }
        [XmlElement("DatumZavrsetka")]
        public string SomeDateString1
        {
            get { return this.DatumZavrsetka.ToString("dd/MM/yyyy"); }
            set {
                    var temp = value.Split('/');
                    var temp1 = temp[0];
                    temp[0] = temp[1];
                    temp[1] = temp1;
                    value = temp[0] + "/" + temp[1] + "/" + temp[2];
                    this.DatumZavrsetka = DateTime.Parse(value);
                }
        }

        public bool Active { get => active; set => active = value; }

        public Aranzman(string naziv, TipAranzmana tip, TipPrevoza prevoz, string lokacija, DateTime datumPocetka, DateTime datumZavrsetka, MestoNalazenja mesto, string vremeNalazenja, int maxPutnika, string opis, string programPutovanja, string path, Smestaj smestaj)
        {
            this.Naziv = naziv;
            this.Tip = tip;
            this.Prevoz = prevoz;
            this.Lokacija = lokacija;
            this.DatumPocetka = datumPocetka;
            this.DatumZavrsetka = datumZavrsetka;
            this.Mesto = mesto;
            this.VremeNalazenja = vremeNalazenja;
            this.MaxPutnika = maxPutnika;
            this.Opis = opis;
            this.ProgramPutovanja = programPutovanja;
            this.Path = path;
            this.Smestaj = smestaj;
        }

        public Aranzman()
        {
            this.Naziv = string.Empty;
            this.Lokacija = string.Empty;
            this.DatumPocetka = new DateTime();
            this.DatumZavrsetka = new DateTime();
            this.Mesto = null;
            this.VremeNalazenja = string.Empty;
            this.MaxPutnika = 0;
            this.Opis = string.Empty;
            this.ProgramPutovanja = string.Empty;
            this.Path = string.Empty;
            this.Smestaj = null;
        }

        public override bool Equals(object obj)
        {
            var aranzman = obj as Aranzman;
            return aranzman != null &&
                   naziv == aranzman.naziv &&
                   tip == aranzman.tip &&
                   prevoz == aranzman.prevoz &&
                   lokacija == aranzman.lokacija &&
                   datumPocetka == aranzman.datumPocetka &&
                   datumZavrsetka == aranzman.datumZavrsetka &&
                   EqualityComparer<MestoNalazenja>.Default.Equals(mesto, aranzman.mesto) &&
                   vremeNalazenja == aranzman.vremeNalazenja &&
                   maxPutnika == aranzman.maxPutnika &&
                   opis == aranzman.opis &&
                   programPutovanja == aranzman.programPutovanja &&
                   path == aranzman.path &&
                   EqualityComparer<Smestaj>.Default.Equals(smestaj, aranzman.smestaj);
        }

        public override int GetHashCode()
        {
            var hashCode = 128145954;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(naziv);
            hashCode = hashCode * -1521134295 + tip.GetHashCode();
            hashCode = hashCode * -1521134295 + prevoz.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(lokacija);
            hashCode = hashCode * -1521134295 + datumPocetka.GetHashCode();
            hashCode = hashCode * -1521134295 + datumZavrsetka.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<MestoNalazenja>.Default.GetHashCode(mesto);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(vremeNalazenja);
            hashCode = hashCode * -1521134295 + maxPutnika.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(opis);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(programPutovanja);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(path);
            hashCode = hashCode * -1521134295 + EqualityComparer<Smestaj>.Default.GetHashCode(smestaj);
            return hashCode;
        }
    }
}