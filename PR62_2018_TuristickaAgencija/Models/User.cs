﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace PR62_2018_TuristickaAgencija.Models
{
    [Serializable]
    public class User
    {
        private string username;
        private string password;
        private string firstName;
        private string lastName;
        private string pol;
        private string email;
        private DateTime datumRodjenja;
        private Uloga uloga;
        private List<Aranzman> listaAranzmana;
        private List<Rezervacija> listaRezervacija;
        private List<string> komentarisane;
        private bool active = false;

        public User()
        {
            this.Username = string.Empty;
            this.Password = string.Empty;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Pol = string.Empty;
            this.Email = string.Empty;
            this.DatumRodjenja = new DateTime();
            this.Active = false;
        }

        public User(string username, string password, string firstName, string lastName, string pol, string email, DateTime datumRodjenja, Uloga uloga, bool active)
        {
            this.Username = username;
            this.Password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Pol = pol;
            this.Email = email;
            this.DatumRodjenja = datumRodjenja;
            this.Uloga = uloga;
            this.CreateList(uloga);
            this.Active = active;
        }

        public void CreateList(Uloga uloga)
        {
            if (uloga == Uloga.MENADZER && this.listaAranzmana == null)
                this.listaAranzmana = new List<Aranzman>();

            else if (uloga == Uloga.TURISTA && this.listaRezervacija == null)
            {
                this.listaRezervacija = new List<Rezervacija>();
                this.Komentarisane = new List<string>();
            }
        }

        public override bool Equals(object obj)
        {
            var user = obj as User;
            return user != null &&
                   username == user.username;
        }

        public override int GetHashCode()
        {
            return 799926177 + EqualityComparer<string>.Default.GetHashCode(username);
        }

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Pol { get => pol; set => pol = value; }
        public string Email { get => email; set => email = value; }  
        [XmlIgnore]
        public DateTime DatumRodjenja { get => datumRodjenja; set => datumRodjenja = value; }
        [XmlElement("DatumRodjenja")]
        public string SomeDateString
        {
            get { return this.DatumRodjenja.ToString("dd/MM/yyyy"); }
            set
            {
                var temp = value.Split('/');
                var temp1 = temp[0];
                temp[0] = temp[1];
                temp[1] = temp1;
                value = temp[0] + "/" + temp[1] + "/" + temp[2];
                this.DatumRodjenja = DateTime.Parse(value);
            }
        }
        public Uloga Uloga { get => uloga; set => uloga = value; }
        public List<Aranzman> ListaAranzmana { get => listaAranzmana; set => listaAranzmana = value; }
        public List<Rezervacija> ListaRezervacija { get => listaRezervacija; set => listaRezervacija = value; }
        public bool Active { get => active; set => active = value; }
        public List<string> Komentarisane { get => komentarisane; set => komentarisane = value; }
    }
}