﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Serialization;

namespace PR62_2018_TuristickaAgencija.Models
{
    public class DB
    {
        public static void Save<T>(List<T> elements)
        {
            if (elements == null) { return; }

            string path = string.Empty;

            if (typeof(T) == typeof(User))
            {
                path = HostingEnvironment.MapPath("~/App_Data/users.txt");
            }
            else if (typeof(T) == typeof(Aranzman))
            {
                path = HostingEnvironment.MapPath("~/App_Data/aranzmani.txt");
            }
            else if (typeof(T) == typeof(Komentar))
            {
                path = HostingEnvironment.MapPath("~/App_Data/komentari.txt");
            }
            else if (typeof(T) == typeof(Smestaj))
            {
                path = HostingEnvironment.MapPath("~/App_Data/smestaji.txt");
            }
            else if (typeof(T) == typeof(Rezervacija))
            {
                path = HostingEnvironment.MapPath("~/App_Data/rezervacije.txt");
            }
            else if (typeof(T) == typeof(Zauzetost))
            {
                path = HostingEnvironment.MapPath("~/App_Data/zauzetost.txt");
            }
            else
            {
                return;
            }

            try
            {
                XmlSerializer serializer = new XmlSerializer(elements.GetType());
                using (StreamWriter sw = new StreamWriter(path))
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        serializer.Serialize(stream, elements);
                        stream.Position = 0;
                        string input = "";
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            input = reader.ReadToEnd();
                        }
                        sw.WriteLine(input);
                        stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static List<T> Load<T>(string path)
        {
            List<T> ret = new List<T>();
            path = HostingEnvironment.MapPath(path);

            try
            {
                string txtDoc = "";
                using (FileStream stream = new FileStream(path, FileMode.Open))
                {
                    StreamReader sr = new StreamReader(stream);
                    string line = "";
                    while ((line = sr.ReadLine()) != null)
                    {
                        txtDoc += line.Trim();
                    }

                    using (StringReader read = new StringReader(txtDoc))
                    {
                        Type outType = typeof(List<T>);

                        XmlSerializer serializer = new XmlSerializer(outType);
                        using (XmlReader reader = new XmlTextReader(read))
                        {
                            ret = (List<T>)serializer.Deserialize(reader);
                            reader.Close();
                        }

                        read.Close();
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return ret;
        }
    }
}