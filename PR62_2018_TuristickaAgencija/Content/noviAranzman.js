﻿function validateAranzman(event) {
    var result = true;

    if (event.submitter.value == "Odustani")
        return result;

    if (document.getElementById("error-msg").innerHTML.trim() == "")
        document.getElementById("error-msg").innerHTML = "Popunite sva polja da bi ste kreirali novi aranzman";

    if (document.getElementById("naziv").value == "" || document.getElementById("naziv").value.trim() == "") {
        document.getElementById("naziv").style.borderColor = "red";
        document.getElementById("naziv").focus();
        result = false;
    } else {
        document.getElementById("naziv").style.borderColor = "green";
    }

    if (document.getElementById("ulica").value == "" || document.getElementById("ulica").value.trim() == "") {
        document.getElementById("ulica").style.borderColor = "red";
        if (result)
            document.getElementById("ulica").focus();
        result = false;
    } else {
        document.getElementById("ulica").style.borderColor = "green";
    }

    if (document.getElementById("broj").value == "") {
        document.getElementById("broj").style.borderColor = "red";
        if (result)
            document.getElementById("broj").focus();
        result = false;
    } else {
        document.getElementById("broj").style.borderColor = "green";
    }

    if (document.getElementById("lokacija").value == "" || document.getElementById("lokacija").value.trim() == "") {
        document.getElementById("lokacija").style.borderColor = "red";
        if (result)
            document.getElementById("lokacija").focus();
        result = false;
    } else {
        document.getElementById("lokacija").style.borderColor = "green";
    }

    if (document.getElementById("gradmesto").value == "" || document.getElementById("gradmesto").value.trim() == "") {
        document.getElementById("gradmesto").style.borderColor = "red";
        if (result)
            document.getElementById("gradmesto").focus();
        result = false;
    } else {
        document.getElementById("gradmesto").style.borderColor = "green";
    }

    if (document.getElementById("post").value == "") {
        document.getElementById("post").style.borderColor = "red";
        if (result)
            document.getElementById("post").focus();
        result = false;
    } else {
        document.getElementById("post").style.borderColor = "green";
    }

    if (document.getElementById("datumPocetka").value == "") {
        if (result == true) {
            document.getElementById("datumPocetka").focus();
        }
        document.getElementById("datumPocetka").style.borderColor = "red";
        result = false;
    } else {
        document.getElementById("datumPocetka").style.borderColor = "green";
    }
    if (document.getElementById("datumZavrsetka").value == "") {
        if (result == true) {
            document.getElementById("datumZavrsetka").focus();
        }
        document.getElementById("datumZavrsetka").style.borderColor = "red";
        result = false;
    } else {
        document.getElementById("datumZavrsetka").style.borderColor = "green";
    }

    var pocMin = document.getElementById("datumPocetka").value.split("-");
    var pocMax = document.getElementById("datumZavrsetka").value.split("-");
    if (parseInt(pocMin[0]) > parseInt(pocMax[0]) || parseInt(pocMin[1]) > parseInt(pocMax[1])) {
        document.getElementById("error-msg").innerHTML = "Datum pocetka mora biti pre datuma zavrsetka putovanja";
        document.getElementById("datumPocetka").style.borderColor = "red";
        document.getElementById("datumZavrsetka").style.borderColor = "red";
        result = false;
    } else if (parseInt(pocMin[1]) == parseInt(pocMax[1]) && parseInt(pocMin[2]) > parseInt(pocMax[2])) {
        document.getElementById("errmsg").innerHTML = "Datum pocetka mora biti pre datuma zavrsetka putovanja";
        document.getElementById("datumPocetka").style.borderColor = "red";
        document.getElementById("datumZavrsetka").style.borderColor = "red";
        result = false;
    } else {
        if (document.getElementById("error-msg").innerHTML == "Datum pocetka mora biti pre datuma zavrsetka putovanja")
            document.getElementById("error-msg").innerHTML = "";
    }

    if (document.getElementById("sat").value == "") {
        document.getElementById("sat").style.borderColor = "red";
        if (result)
            document.getElementById("sat").focus();
        result = false;
    } else {
        document.getElementById("sat").style.borderColor = "green";
    }

    if (document.getElementById("min").value == "") {
        document.getElementById("min").style.borderColor = "red";
        if (result)
            document.getElementById("min").focus();
        result = false;
    } else {
        document.getElementById("min").style.borderColor = "green";
    }

    if (document.getElementById("maxPutnika").value == "") {
        document.getElementById("maxPutnika").style.borderColor = "red";
        if (result)
            document.getElementById("maxPutnika").focus();
        result = false;
    } else {
        document.getElementById("maxPutnika").style.borderColor = "green";
    }

    if (document.getElementById("geoSirina").value == "" || document.getElementById("geoDuzina").value == "") {
        if (document.getElementById("error-msg").innerHTML.trim() == "" || document.getElementById("error-msg").innerHTML.trim() == "Popunite sva polja da bi ste kreirali novi aranzman")
            document.getElementById("error-msg").innerHTML = "Odaberite lokaciju na mapi";
        result = false;
    } else {
        if (document.getElementById("error-msg").innerHTML == "Odaberite lokaciju na mapi")
            document.getElementById("error-msg").innerHTML = "";
    }

    if (document.getElementById("opis").value == "" || document.getElementById("opis").value.trim() == "") {
        document.getElementById("opis").style.borderColor = "red";
        if (result)
            document.getElementById("opis").focus();
        result = false;
    } else {
        document.getElementById("opis").style.borderColor = "green";
    }

    if (document.getElementById("programPutovanja").value == "" || document.getElementById("programPutovanja").value.trim() == "") {
        document.getElementById("programPutovanja").style.borderColor = "red";
        if (result)
            document.getElementById("programPutovanja").focus();
        result = false;
    } else {
        document.getElementById("programPutovanja").style.borderColor = "green";
    }

    if (event.submitter.value != "Izmeni") {
        if (document.getElementById("file").files.length == 0) {
            if (document.getElementById("error-msg").innerHTML.trim() == "" || document.getElementById("error-msg").innerHTML.trim() == "Popunite sva polja da bi ste kreirali novi aranzman")
                document.getElementById("error-msg").innerHTML = "Niste dodali sliku";
            result = false;
        }
        else {
            if (document.getElementById("error-msg").innerHTML == "Niste dodali sliku")
                document.getElementById("error-msg").innerHTML = "";
        }
    }

    if (document.getElementById("smestaj").value == "" || document.getElementById("smestaj").value.trim() == "") {
        document.getElementById("smestaj").style.borderColor = "red";
        if (result)
            document.getElementById("smestaj").focus();
        result = false;
    } else {
        var tmp = false;
        for (var i = 0; i < jScriptArray.length; i++) {
            if (jScriptArray[i] == document.getElementById("smestaj").value) {
                tmp = true;
                break;
            }
        }
        if (tmp == true)
            document.getElementById("smestaj").style.borderColor = "green";
        else {
            document.getElementById("smestaj").style.borderColor = "red";
        }
    }

    return result;
}