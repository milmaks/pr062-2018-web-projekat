﻿using PR62_2018_TuristickaAgencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PR62_2018_TuristickaAgencija
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            List<User> users = DB.Load<User>("~/App_Data/users.txt");
            HttpContext.Current.Application["users"] = users;

            List<Aranzman> arazmani = DB.Load<Aranzman>("~/App_Data/aranzmani.txt");
            HttpContext.Current.Application["aranzmani"] = arazmani;

            List<Komentar> komentari = DB.Load<Komentar>("~/App_Data/komentari.txt");
            HttpContext.Current.Application["komentari"] = komentari;

            List<Rezervacija> rezervacije = DB.Load<Rezervacija>("~/App_Data/rezervacije.txt");
            foreach (var rez in rezervacije)
            {
                if (rez.Aranzman.DatumZavrsetka < DateTime.Now.Date)
                    rez.Status = Status.ZAVRSENA;
            }
            DB.Save<Rezervacija>(rezervacije);
            HttpContext.Current.Application["rezervacije"] = rezervacije;

            List<Smestaj> smestaji = DB.Load<Smestaj>("~/App_Data/smestaji.txt");
            HttpContext.Current.Application["smestaji"] = smestaji;
            HttpContext.Current.Application["smestajnejed"] = smestaji;

        }
    }
}
